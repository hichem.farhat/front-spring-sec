import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users:any=[]
constructor(private serv:GestionService) { }
ngOnInit() {
this.getalluser();
}
getalluser(){
this.serv.getalluser().subscribe(
data => { this.users=data ; console.log(data) },
err => { console.log(err) }
)
}

}
