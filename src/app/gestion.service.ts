import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
@Injectable({
  providedIn: 'root'
})
export class GestionService {
username:string;
token:string;

  constructor(private http:HttpClient) { }
  login(request){
    return this.http.post("http://127.0.0.1:8080/auth/login",request,{observe:'response'})
    }
    getalluser(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/auth/getall",{headers:headers});
      }
      saveToken(token){
        let helper = new JwtHelperService();
        this.token=token;
        let decodedToken = helper.decodeToken(token);
        this.username=decodedToken.sub;
        // recuperer le username
        }
}
