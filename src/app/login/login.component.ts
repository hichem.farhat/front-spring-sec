import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  response:any={"token":""}
  constructor(private gestion:GestionService, private route:Router) { }

  ngOnInit() {
  }
  connexion(request){
    this.gestion.login(request).subscribe(
    data => {
    this.response=data.body;
    let token=this.response.token;
    localStorage.setItem("token",token)
    this.gestion.saveToken(token)
    this.route.navigate(['/users']);
    },
    err => {
    console.log(err)
    }
    )
    }

}
